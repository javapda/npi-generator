#! /usr/bin/env node

const { Command, Option } = require('commander');

const chalk = require('chalk');
const programVersion=require('./package.json').version
const programName=require('./package.json').name
let numberOfNpis=1;
let verbose=true

const NpiGenerator = require('./src/npi-generator');
exports.NpiGenerator = NpiGenerator; 
const program = new Command();
program.description(`program name is ${chalk.bold.yellow(programName)}`);
program.version(programVersion);
program.addOption(new Option('-v, --verbose', 'timeout in seconds').default(verbose, `default is ${verbose}`));
program.addOption(new Option('-n, --number <count>', `number ${chalk.bold.blue('(NPI=National Provider Identifier)')} of npi(s) to output`).default(numberOfNpis, 'one NPI'))
// program.option('-d, --debug', 'output extra debugging');
program.parse(process.argv);
const options = program.opts();
if (options.number) {
  numberOfNpis=options.number;
}

for (let i = 0 ; i < numberOfNpis; i++) {
  console.log(new NpiGenerator().generate());
}

