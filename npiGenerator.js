#!/usr/bin/env node

/**
* adapted from "https://jsfiddle.net/alexdresko/cLNB6/"
*/
const Npi = class {
  randomNumber9Digits() {return Math.floor(Math.random()*(999999999-100000000+1)+100000000);}
  checkDigitNpi = (npi9) => {
    let tmp,sum,i,j;
    npi9=""+npi9;
    i = npi9.length;
    const med = npi9.length;
    if ((i == 14) && (npi9.indexOf("80840", 0, 5) == 0))
      sum = 0;
    else if (i == 9)
      sum = 24;
    else {
      return "!";
    }

    j = 1;
    while (i != 0) {
        tmp = npi9.charCodeAt(i - 1) - '0'.charCodeAt(0);
        if ((j++ % 2) != 0) {
            if ((tmp <<= 1) > 9) {
                tmp -= 10;
                tmp++;
            }
        }
        sum += tmp;
        i--;
    }
    return String.fromCharCode(((10 - (sum % 10)) % 10 + 48));

  }
  generate() {
    var randomNumber = this.randomNumber9Digits();
    const asdf = this.checkDigitNpi(randomNumber.toString());
    const npi=randomNumber.toString() + asdf.toString();
    return npi;
  }
};

const _usage = () => {
  console.log(`
  
  USAGE
     ${process.argv[1]} [option]

  OPTIONS
     -h | --help        show help
  
  DESCRIPTION
     * by default, running ${process.argv[1]} will output an NPI (National Provider Identifier)

  `);
}

if (process.argv.length == 3) {
  let arg=process.argv[2];
  switch(arg) {
    case "-h":
    case "--help":
      _usage();
      break;
    default:
      console.error(`don't know how to process '${arg}'`)
  }  
} else {
  console.log(new Npi().generate());
}