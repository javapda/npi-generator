# [npi-generator](https://bitbucket.org/javapda/npi-generator)

adapted from [npi generator jsfiddle](https://jsfiddle.net/alexdresko/cLNB6/)

# Installation / setup #

## Install ##
  ```
  # will install to /usr/local/lib/node_modules/npi-generator
  sudo npm install --global npi-generator
  ```

## Update npi-generator ##
* periodically a new version of this utility will be released, run the following to update:
```
npm update -g npi-generator
```

## cli usage ##
```
npi-generator --help
```


## usage ##
```
const {NpiGenerator} = require('npi-generator')

let npi = new NpiGenerator().generate();
console.log(`${npi}`);
```
## output (example) ##
```
8558316757
```


# Links #
* [NPI Standard](https://www.cms.gov/Regulations-and-Guidance/Administrative-Simplification/NationalProvIdentStand)
* [npi registry](https://npiregistry.cms.hhs.gov/)
* [npi-lookup](https://npi-lookup.org/)
* [npnumber-lookup](https://www.npinumberlookup.org/)

# publish package #
```
npm login
npm publish --access public
```

# libraries/packages #
* [commander for CLI](https://www.npmjs.com/package/commander) | [commander on github](https://github.com/tj/commander.js)
* [chalk for CLI](https://www.npmjs.com/package/chalk) | [chalk on github](https://github.com/chalk/chalk)
* [jest for testing](https://jestjs.io/)
  
# How to publish
```
// after git committing
npm version patch
npm publish --access public
```
