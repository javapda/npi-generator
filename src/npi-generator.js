module.exports = class NpiGenerator {
  constructor() {
    // console.log(`generating ${this}`);
  }
  randomNumber9Digits() {return Math.floor(Math.random()*(999999999-100000000+1)+100000000);}
  checkDigitNpi = (npi9) => {
    let tmp,sum,i,j;
    npi9=""+npi9;
    i = npi9.length;
    const med = npi9.length;
    if ((i == 14) && (npi9.indexOf("80840", 0, 5) == 0))
      sum = 0;
    else if (i == 9)
      sum = 24;
    else {
      return "!";
    }

    j = 1;
    while (i != 0) {
        tmp = npi9.charCodeAt(i - 1) - '0'.charCodeAt(0);
        if ((j++ % 2) != 0) {
            if ((tmp <<= 1) > 9) {
                tmp -= 10;
                tmp++;
            }
        }
        sum += tmp;
        i--;
    }
    return String.fromCharCode(((10 - (sum % 10)) % 10 + 48));

  }
  generate() {
    var randomNumber = this.randomNumber9Digits();
    const asdf = this.checkDigitNpi(randomNumber.toString());
    const npi=randomNumber.toString() + asdf.toString();
    return npi;
  }
}